`patch default.nix universal-1.patch`

`patch haskell/shpaboinchkle.cabal universal-2.patch`

If there's a chan error

`patch default.nix chan-error.pach`

To fix the haskell compiler errors

`patch haskell/src/Main.hs haskell.patch`
